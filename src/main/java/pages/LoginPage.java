package pages;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class LoginPage {
	WebDriver driver;
	public LoginPage(WebDriver driver) {
		this.driver=driver;
	}
	public By usernameTextbox() {
		return By.id("email");
	}
	public By passwordTextBox() {
		return By.id("pass");
	}
	public By loginButton() {
		return By.name("login");
	}
	public By errorPage() {
		return By.id("error_box");
	}
	public void enterCredentials(String username,String password) {
		driver.findElement(usernameTextbox()).sendKeys(username);
		driver.findElement(passwordTextBox()).sendKeys(password);
	}
	public void clickLoginButton() {
		driver.findElement(loginButton()).click();
	}
	public void validateErrorMessage() {
		driver.findElement(errorPage()).isDisplayed();
		Assertions.assertEquals(driver.findElement(errorPage()).isDisplayed(), false);
	}
}
